Name:           pingu
Version:        1.5
Release:        1%{?dist}
Summary:        Policy routing daemon with fail-over and load-balancing for multi ISP setups

License:        GPL+
URL:            https://github.com/ncopa/%{name}
Source0:        https://github.com/ncopa/%{name}/archive/v%{version}.tar.gz
Source1:        pingu.service

BuildRequires:  libev-devel kernel-headers asciidoc systemd
Requires:       systemd

%description
A daemon that takes care of policy routing and fail-over in multi ISP setups.
Features:
    Support for DHCP
    Support for PPP
    ISP fail-over
    Load-balancing (next-hop)
    Optional route rule based on fwmark
    run script when ISP goes up/down

%prep
%autosetup

%build
./configure --prefix=%{_prefix} --exec-prefix=%{_exec_prefix} --bindir=%{_bindir} --sbindir=%{_sbindir} --sysconfdir=%{_sysconfdir} --datarootdir=%{_datadir} --libdir=%{_libdir} --localstatedir=%{_localstatedir} --mandir=%{_mandir}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
%make_install
install -m644 -D %{name}.conf %{buildroot}/%{_sysconfdir}/%{name}/%{name}.conf
install -m644 -D %{SOURCE1} %{buildroot}/%{_unitdir}/%{name}.service
chmod 644 %{buildroot}/%{_mandir}/{man5/pingu.conf.5,man8/{pingu.8,pinguctl.8}} # fix spurious-executable-perm; a proper fix would be a makefile patch

%files
%{_sbindir}/%{name}
%{_sbindir}/%{name}ctl
%{_bindir}/mtu
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/%{name}/%{name}.conf
%doc %{_mandir}/man5/pingu.conf.5.gz
%doc %{_mandir}/man8/pingu.8.gz
%doc %{_mandir}/man8/pinguctl.8.gz

%changelog
* Thu Mar 22 2018 wldhx <wldhx@wldhx.me> - 1.5.1
- Initial release
